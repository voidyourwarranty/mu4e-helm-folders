# mu4e-helm-folders : Helm based folder selection functions for the Emacs email system mu4e

This is an Emacs Lisp project that provides mail folder selection functions for the email program
[mu4e](https://www.djcbsoftware.nl/code/mu/mu4e.html) by Dirk-Jan C. Binnema that are based on
[helm](https://github.com/emacs-helm/helm) by Thierry Volpiatto.

# Features

## Jump to Folder

The function `mf-jump-to-maildir` uses `helm` to select a mail folder (aka maildir) and jumps to that folder.

## Mark Message and Set Move Target

The internal function `mu4e~mark-get-move-target` is replaced by a function that used `helm` in order to select the move
target when marking messages for moving in the header mode of `mu4e`.

## Bash-Like Behaviour of the Tab Key

Helm is great, but after working with bash for 30+ years, my bone marrow always makes me hit [TAB] in order to
auto-complete. With the standard key bindings of helm, however, this gets me into the menu that selects the helm action
and always results in a mess.

Therefore, we add a persistent helm action that can be bound to the [TAB] key. This persistent action mimicks tab
auto-completion of bash. It considers all existing mail folders (aka maildirs) and completes as much of the folder path
as is uniquely specified given the current user input in the minibuffer.

This way, hitting [TAB] becomes productive and no longer messes up helm.

# Configuration

In order to load `mu4e-helm-folders`, you place the files `mu4e-helm-folders.el` in the
load-path and
```
(require 'mu4e-helm-folders)
```

An example configuration with suggested key bindings is as follows,
```
;; Replace the [j]ump to folder command by our new function.
(define-key mu4e-main-mode-map "j"    'mf-jump-to-maildir)
(define-key mu4e-headers-mode-map "j" 'mf-jump-to-maildir)
(define-key mu4e-view-mode-map "j"    'mf-jump-to-maildir)

;; Optional: replace the standard helm behaviour by one that is closer to [TAB] completion in bash.
(define-key helm-map (kbd "<tab>")   'helm-execute-persistent-action) ;; tab key in a helm selection
(define-key helm-map (kbd "C-i")     'helm-execute-persistent-action) ;; tab key if Emacs in run in a terminal
(define-key helm-map (kbd "<C-tab>") 'helm-select-action)             ;; this used to be bound to [TAB]
(define-key helm-map (kbd "C-j")     'helm-select-action)             ;; [C-j] used to be helm-execute-persistent-action
```

# Internals

## Name space

All variables and functions defined by `mu4e-helm-folders` use the prefix `mf-`.

## Modifications of existing mu4e functions

In order to replace `mu4e`'s selection of move targets by our new helm based function, one of the functions of `mu4e` is
brutally overwritten. This is the function `mu4e~mark-get-move-target`. Should `mu4e`'s behaviour change in a future
version, there is no guarantee that our modifications will continue to work.

## Modification of Tab Behaviour

Note that binding the [TAB] key to the persistent action as suggected in the configuration above, affects all helm
sources, not merely the ones defined in the present module.

## Tasks and Suggestions

### Manadatory

- whenever `mu4e` re-indexes the tree of mail folders, we need to `(setq mf-touch-maildirs t)` in order to update the
internal structure that describes all maildirs.

## License

[GNU General Public License GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)
