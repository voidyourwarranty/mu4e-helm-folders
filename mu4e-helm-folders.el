;;; -*- mode:emacs-lisp -*-
;;; ================================================================================
;;; File:    mu4e-helm-folders.el
;;; Date:    2020-10-11
;;; Author:  Void Your Warranty
;;; License: Gnu General Public License GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
;;; Purpose: Folder selection in mu4e using helm.
;;; ================================================================================

(require 'mu4e)
(require 'org-mu4e)

(require 'helm)
(require 'helm-config)

;; Define a helm persistent action that provides a bash style [TAB] completion of maildirs.
;; ----------------------------------------------------------------------------------------

(defun mf-common-prefix-two (a b)
  "Return the longest common prefix of the two given strings."
  (let ((result (compare-strings a nil nil b nil nil)))
    (if (numberp result)
	(substring a 0 (- (abs result) 1))
      a)))

(defun mf-common-prefix-list (l)
  "Return the longest common prefix of the strings in the given list."
  (if (not l) ""
    (let ((common (car l)))
      (catch 'result
	(dolist (try (cdr l))
	  (setq common (mf-common-prefix-two common try))
	  (when (string= common "")
	    (throw 'result ""))))
      common)))

(defvar mf-global-maildirs nil
  "List of all maildirs with the absolute paths. Needs to be
  recomputed whenever the user has created or deleted a
  maildir.")

(defvar mf-touch-maildirs t
  "If <true>, the maildirs need to be recomputed.")

(defun mf-refresh-global-maildirs ()
  "Recompute the list of all maildirs."
  (when mf-touch-maildirs
    (setq mf-global-maildirs (sort (mu4e-get-maildirs) 'string<))
    (setq mf-touch-maildirs nil)))

(defun mf-maildir-persistent-action (candidate search)
  "[TAB] completion during the interactive helm session with
  maildirs is implemented by this persistent action. If <search>
  is true and [TAB] completion yields a unique maildir, this
  maildir is already displayed."
  (let* (
	 ;; what the user typed into the helm search line
	 (typed helm-pattern)

	 ;; all mail folders whose beginning agrees with what the user typed
	 (matches  (remove nil (mapcar (lambda (x) (when (string-prefix-p typed x) x)) mf-global-maildirs)))

	 ;; local function that replaces the search string in the helm minibuffer
	 (insert-in-minibuffer (lambda (fname)
				 (with-selected-window (or (active-minibuffer-window) (minibuffer-window))
				   (delete-minibuffer-contents)
				   (set-text-properties 0 (length fname) nil fname)
				   (insert fname)))))

    ;; If the user has typed in something entirely new, there is not even a helm candidate, and this persistent action
    ;; function is never called. The fact that it is called implies that there is a valid candidate (the currently
    ;; highlighted line).

    (when (not matches)
      (when (string-match (regexp-quote typed) candidate)

	;; If the search term in the helm minibuffer is not a prefix of any mail folder name, but a proper substring, we
	;; fill in everything to the left of the substring from the best match, then recompute the matched and later
	;; perform all the other completion methods.
	;; This is a new completion policy that the user may or may not like!

	(progn
	  (setq typed    (concat (substring candidate 0 (string-match (regexp-quote typed) candidate)) typed))
	  (setq matches  (remove nil (mapcar (lambda (x) (when (string-prefix-p typed x) x)) mf-global-maildirs)))
	  (funcall insert-in-minibuffer typed))))

    (when matches
      (if (= 1 (length matches))
	  (progn
	    (funcall insert-in-minibuffer (car matches))
	    (when search
		(mu4e-headers-search (format "maildir:\"%s\"" (car matches)))))
	(progn
	  (setq test (mf-common-prefix-list matches))
	  (funcall insert-in-minibuffer test))))))

;; Replace [j] "jump to maildir" with a helm selection of folders.
;; --------------------------------------------------------------------------------

(defun mf-jump-action (candidate)
  "Helm action in order to jump to the selected maildirs."
  (mu4e-headers-search (concat "maildir:\"" (mapconcat 'identity (helm-marked-candidates :all-sources t) "\" OR maildir:\"") "\"")))

(defun mf-jump-to-maildir ()
  "Helm source for jumping to one or several selected
  maildirs. The persistent action provides [TAB] completion."
  (interactive)
  (mf-refresh-global-maildirs)
  (helm :sources (helm-build-sync-source "Jump to Mail Folder"
		   :candidates mf-global-maildirs
		   :fuzzy-match t
		   :action #'mf-jump-action
		   :persistent-action #'(lambda (candidate) (mf-maildir-persistent-action candidate t)))
	:buffer "*helm jump to maildir*"))

;; Replace the mail move marker [m] from mu4e-mark.el by a helm selection of folders.
;; --------------------------------------------------------------------------------

;; The following function from helm-mark.el is brutally overwritten. This approach may break in future versions of mu4e.

(defun mu4e~mark-get-move-target ()
  "Helm source to select a maildir target for a move
  operation. The persistent action provides [TAB]
  completion. Quitting the selection with [RET] selects the
  currently highlighted line from the helm suggestions. Quitting
  with [C-RET] selects the current user input as the name of a
  new maildir to create."
  (interactive)
  (mf-refresh-global-maildirs)
  (let* ((result nil)
	 (set-result (lambda (value) (setq result value))))
    (helm :sources (list (helm-build-sync-source "Move to Mail Folder"
			   :candidates mf-global-maildirs
			   :fuzzy-match t
			   :action #'(lambda (candidate) (funcall set-result candidate))
			   :persistent-action #'(lambda (candidate) (mf-maildir-persistent-action candidate nil)))
			 (helm-build-sync-source "Create New Mail Folder"
                           :candidates (lambda () (list helm-pattern))
                           :volatile t
                           :action #'(lambda (candidate) (funcall set-result candidate))))
	  :buffer "*helm get move target*")
    (if (not result)
	(setq quit-flag t) ;; this is how marking can be aborted in a clean fashion
      (let ((fulltarget (concat mu4e-maildir result)))
	(if (file-directory-p fulltarget)
	    result
	  (if (yes-or-no-p (format "%s does not exist.  Create now?" fulltarget))
	      (progn
		(mu4e~proc-mkdir fulltarget)
		(setq mf-touch-maildirs t)
		result)
	    (progn
	      (setq quit-flag t)
	      nil)))))))

(provide 'mu4e-helm-folders)

;;; ================================================================================
;;; End of file.
;;; ================================================================================
